//--------------------------- Testing Problem 2 statement: Number of matches won per team per year in IPL.. -------------------------------------


const {matchesWonPerTeamPerYear} = require('../server/problemStatementCode/matchesWonPerTeam.js')
const {testMatchData} =require('./testData/testMatchesData.js') 



test("Testing for correct output getting a object with year and team won count", ()=>{
    expect(matchesWonPerTeamPerYear(testMatchData)).toEqual({'2013':{'Rajasthan Royals' :1},'2017':{"Kolkata Knight Riders":2} })
})


test("Testing for null input", ()=>{
    expect(matchesWonPerTeamPerYear(null)).toThrow('You are using wrong (null) input data type')
})
test("Testing for number input", ()=>{
    expect(matchesWonPerTeamPerYear(1)).toThrow('You are using wrong (number) input data type')
})
test("Testing for undefined input", ()=>{
    expect(matchesWonPerTeamPerYear(undefined)).toThrow('You are using wrong (undefined) input data type')
})
test("Testing for empty string input", ()=>{
    expect(matchesWonPerTeamPerYear('')).toThrow('You are using wrong (empty) input data type')
})
test("Testing for boolean input", ()=>{
    expect(matchesWonPerTeamPerYear(true)).toThrow('You are using wrong (boolean) input data type')
})
test("Testing for falsy input", ()=>{
    expect(matchesWonPerTeamPerYear(0)).toThrow('You are using wrong (falsy zero) input data type')
})
test("Testing for empty array input", ()=>{
    expect(matchesWonPerTeamPerYear([])).toThrow('You are using wrong (empty array) input data type')
})
test("Testing for empty object input", ()=>{
    expect(matchesWonPerTeamPerYear({})).toThrow('You are using wrong (empty object) input data type')
})

