//--------------------------- Testing Problem 1 statement: Number of matches played per year for all the years in IPL. -------------------------------------


const {matchesPlayedPerYear} = require('../server/problemStatementCode/matchesPlayedPerYear.js')
const {testMatchData} =require('./testData/testMatchesData.js') 


test("Testing for correct input value", ()=>{
    expect(matchesPlayedPerYear(testMatchData)).toEqual({'2013':1,'2017':2})
})


test("Testing for null input", ()=>{
    expect(matchesPlayedPerYear(null)).toThrow('You are using wrong (null) input data type')
})
test("Testing for number input", ()=>{
    expect(matchesPlayedPerYear(1)).toThrow('You are using wrong (number) input data type')
})
test("Testing for undefined input", ()=>{
    expect(matchesPlayedPerYear(undefined)).toThrow('You are using wrong (undefined) input data type')
})
test("Testing for empty string input", ()=>{
    expect(matchesPlayedPerYear('')).toThrow('You are using wrong (empty) input data type')
})
test("Testing for boolean input", ()=>{
    expect(matchesPlayedPerYear(true)).toThrow('You are using wrong (boolean) input data type')
})
test("Testing for falsy input", ()=>{
    expect(matchesPlayedPerYear(0)).toThrow('You are using wrong (falsy zero) input data type')
})
test("Testing for empty array input", ()=>{
    expect(matchesPlayedPerYear([])).toThrow('You are using wrong (empty array) input data type')
})
test("Testing for empty object input", ()=>{
    expect(matchesPlayedPerYear({})).toThrow('You are using wrong (empty object) input data type')
})

