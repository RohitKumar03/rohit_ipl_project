//--------------------------- Testing Problem 4 statement: Top 10 economical bowlers in the year 2015. -------------------------------------
const {topTenBestEconomy} = require('../server/problemStatementCode/topTenEconomicalBowler.js')
const {testMatchData,deliveryTestData} =require('./testData/testMatchesData.js') 



test("Top ten econoical bowler list in ascending order", ()=>{
    expect(topTenBestEconomy(testMatchData,deliveryTestData,2017)).toEqual([ 'A Choudhary', 'Dhoni' ])
})


test("Testing for null input", ()=>{
    expect(topTenBestEconomy(null)).toThrow('You are using wrong (null) input data type')
})
test("Testing for number input", ()=>{
    expect(topTenBestEconomy(1)).toThrow('You are using wrong (number) input data type')
})
test("Testing for undefined input", ()=>{
    expect(topTenBestEconomy(undefined)).toThrow('You are using wrong (undefined) input data type')
})
test("Testing for empty string input", ()=>{
    expect(topTenBestEconomy('')).toThrow('You are using wrong (empty) input data type')
})
test("Testing for boolean input", ()=>{
    expect(topTenBestEconomy(true)).toThrow('You are using wrong (boolean) input data type')
})
test("Testing for falsy input", ()=>{
    expect(topTenBestEconomy(0)).toThrow('You are using wrong (falsy zero) input data type')
})
test("Testing for empty array input", ()=>{
    expect(topTenBestEconomy([])).toThrow('You are using wrong (empty array) input data type')
})
test("Testing for empty object input", ()=>{
    expect(topTenBestEconomy({})).toThrow('You are using wrong (empty object) input data type')
})