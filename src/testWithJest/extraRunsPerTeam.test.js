//--------------------------- Testing Problem 3 statement: Extra runs conceded per team in the year 2016. -------------------------------------

const {extraRunsPerTeam} = require('../server/problemStatementCode/extraRunsConcededPerTeam.js')
const {testMatchData,deliveryTestData} =require('./testData/testMatchesData.js') 



test("Extra runs per team in the given year in the form of object with property team name with extra runs",
 ()=>{
expect(extraRunsPerTeam(testMatchData,deliveryTestData,2017)).toEqual({'Royal Challengers Bangalore': 14})
})
test("Testing for null input", ()=>{
    expect(extraRunsPerTeam(null)).toThrow('You are using wrong (null) input data type')
})
test("Testing for number input", ()=>{
    expect(extraRunsPerTeam(1)).toThrow('You are using wrong (number) input data type')
})
test("Testing for undefined input", ()=>{
    expect(extraRunsPerTeam(undefined)).toThrow('You are using wrong (undefined) input data type')
})
test("Testing for empty string input", ()=>{
    expect(extraRunsPerTeam('')).toThrow('You are using wrong (empty) input data type')
})
test("Testing for boolean input", ()=>{
    expect(extraRunsPerTeam(true)).toThrow('You are using wrong (boolean) input data type')
})
test("Testing for falsy input", ()=>{
    expect(extraRunsPerTeam(0)).toThrow('You are using wrong (falsy zero) input data type')
})
test("Testing for empty array input", ()=>{
    expect(extraRunsPerTeam([])).toThrow('You are using wrong (empty array) input data type')
})
test("Testing for empty object input", ()=>{
    expect(extraRunsPerTeam({})).toThrow('You are using wrong (empty object) input data type')
})
