const filePathInfo = {
       //----------------csv and json file path ------------------------------------------------------
        csvMatchesFilePath: '../../data/matches.csv',
        csvDeliveriesFilePath: `${__dirname}/../../data/deliveries.csv`,
        matchesJSONFilePath: `${__dirname}/../../data/matches.json`,
        deliveriesJSONFilePath: `${__dirname}/../../data/deliveries.json`,

        //-----------------Output of four problem statements--------------------------------------------
        matchesPlayedPerYearFilePath: "../../public/output/matchesPerYear.json",
        macthesWonPerTeamFilePath: "../../public/output/matchesWonPerTeamYear.json",
        extraRunsPerTeamFilePath: "../../public/output/extraRunsPerTeam.json",
        topTenEconomicalBolwerFilePath: "../../public/output/topTenEconomicalBowler.json",

      //-------------------Four problem statements method path----------------------------------------------  
         matchesPlayedPerYearMethodPath    : `${__dirname}/../problemStatementCode/matchesPlayedPerYear.js`,
         matchesWonPerYearMethodPath      : `${__dirname}/../problemStatementCode/matchesWonPerTeam.js`,
         extraRunsPerTeamMethodPath       : `${__dirname}/../problemStatementCode/extraRunsConcededPerTeam.js`,
         topTenEconomicalBowlerMethodPath : `${__dirname}/../problemStatementCode/topTenEconomicalBowler.js`,

      };
    
      module.exports = { filePathInfo };
 
