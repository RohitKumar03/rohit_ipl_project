const fs = require("fs");
const { filePathInfo } = require("../server/filePath/filePathInfo.js");

const matchesObject = require(filePathInfo["matchesJSONFilePath"]);
const deliveryData = require(filePathInfo["deliveriesJSONFilePath"]);

const {matchesPlayedPerYear} = require(filePathInfo['matchesPlayedPerYearMethodPath'])
const {matchesWonPerTeamPerYear} = require(filePathInfo['matchesWonPerYearMethodPath']);
const {extraRunsPerTeam} = require(filePathInfo['extraRunsPerTeamMethodPath']);
const {topTenBestEconomy} = require(filePathInfo['topTenEconomicalBowlerMethodPath']);

/*------------Number of matches played per year for all the years in IPL. ----------------*/

try {
  let matchesPlayedPerYearResult = matchesPlayedPerYear(matchesObject);
  fs.writeFileSync(
    "../public/output/matchesPerYear.json",
    JSON.stringify(matchesPlayedPerYearResult),
    "utf-8"
  );
} catch (err) {
  console.log(err);
}

/*-------------- Number of matches won per team per year in IPL.-----------*/

try {
  let matchesWonPerTeamPerYearResult = matchesWonPerTeamPerYear(matchesObject);

  fs.writeFileSync(
    "../public/output/matchesWonPerTeamYear.json",
    JSON.stringify(matchesWonPerTeamPerYearResult),
    "utf-8"
  );
} catch (error) {
  console.log(error);
}

/*-------------- Extra runs conceded per team in the year 2016-----------*/

try {
  let year = 2016;
  let extraRunsPerTeamResult = extraRunsPerTeam(
    matchesObject,
    deliveryData,
    year
  );

  fs.writeFileSync(
    "../public/output/extraRunsPerTeam.json",
    JSON.stringify(extraRunsPerTeamResult),
    "utf-8"
  );
} catch (err) {
  console.log(error);
}

/*-------------- Top 10 economical bowlers in the year 2015-----------*/


try {
  let targetYear = 2015;
  fs.writeFileSync(
    "../public/output/topTenEconomicalBowler.json",
    JSON.stringify(topTenBestEconomy(matchesObject, deliveryData, targetYear)),
    "utf-8"
  );
} catch (error) {
  console.log(error);
}
