let {filePathInfo} = require("../filePath/filePathInfo.js");
let matchesObject = require(filePathInfo["matchesJSONFilePath"])

const trackYearId = (matchesObject, year) => {
    if (typeof matchesObject !== "object" || typeof year !== "number") {
      throw new Error("Input data type error");
    }
  
    let matchYearId = matchesObject
      .filter((element) => {
       return (Number(element["season"]) === year) 
      })
      .map((element) => element["id"]);
    return matchYearId;
  };

  module.exports = {trackYearId}