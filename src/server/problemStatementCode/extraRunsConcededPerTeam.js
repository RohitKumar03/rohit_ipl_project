let { filePathInfo } = require("../filePath/filePathInfo.js");
let matchesObject = require(filePathInfo["matchesJSONFilePath"]);
let deliveryData = require(filePathInfo["deliveriesJSONFilePath"]);
const { trackYearId } = require("../helperFunction/trackYearId.js");

// For finding extra runs from delieryData we need to first locate year, which is not there in delivery data
//Find year 2016 in matchesObject and store all ids of 2016 object;
//Then match ids of both matches and delivery object if it conatians then find bowling team and extra runs and store result in new object

const extraRunsPerTeam = (matchesObject, deliveryData, year) => {
  if (
    typeof matchesObject !== "object" ||typeof year !== "number" ||typeof deliveryData !== "object") {
    throw new Error("Input data type error");
  }

    let matchesId = trackYearId(matchesObject, year);
    let extraRuns = deliveryData.reduce((extraRuns, deliveryElement) => {
    let { match_id } = deliveryElement;
    if (matchesId.includes(match_id)) {
      let { bowling_team: bowlingTeam, extra_runs: extraRunsPerTeam } = deliveryElement;
      extraRuns.hasOwnProperty(bowlingTeam) ? (extraRuns[bowlingTeam] += Number(extraRunsPerTeam)): (extraRuns[bowlingTeam] = Number(extraRunsPerTeam));
    }
    return extraRuns;
  }, {});

  return extraRuns;
};


module.exports = { extraRunsPerTeam };
