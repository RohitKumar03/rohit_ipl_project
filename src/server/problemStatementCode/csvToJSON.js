const csvToJson = require("csvtojson");
const csvFilePath = `${__dirname}/../../data/matches.csv`;
const csvDeliveryPath = `${__dirname}/../../data/deliveries.csv`;
const fs = require("fs");

/* ------------  Converting Matches CSV file to JSON using csvtojson library  --------------*/

const csvToJsonConverter = (filePath, fileName)=>{
  csvToJson()
  .fromFile(filePath)
  .then((json) => {
    fs.writeFileSync(fileName, JSON.stringify(json), "utf-8", (err) => {
      if (err) {
        console.log(err);
      }
    });
  });

}

csvToJsonConverter(csvFilePath,"../../data/matches.json")
csvToJsonConverter(csvDeliveryPath,"../../data/deliveries.json")
