/* ------------ Problem statement 4: Top 10 economical bowlers in the year 2015.--------------*/

const { filePathInfo } = require("../filePath/filePathInfo.js");
const matchesObject = require(filePathInfo["matchesJSONFilePath"]);
const deliveryData = require(filePathInfo["deliveriesJSONFilePath"]);
const { trackYearId } = require("../helperFunction/trackYearId.js");

// Approach: First get the year from matchesObject data and put all the bowlers data in an object, with properties of runs ans balls;
// Then iterate over object and calculate economical and push to an array ,and sort the array in ascending order iterate over first ten bowlers

//Creating Object which has bowlers details number of balls bowled, and number of runs conceeded

const economicalBowler = (matchesObject, deliveryData, year) => {
  if (typeof matchesObject !== "object" ||typeof year !== "number" ||typeof deliveryData !== "object") {
    throw new Error("Input data type error");
  }
  let targetBowlerYearId = trackYearId(matchesObject, year);

  economicalData = deliveryData.reduce((economicalData, currentElement) => {
    let { match_id } = currentElement;

    if (targetBowlerYearId.includes(match_id)) {
      //using destructuring getting the values of following in below line

      let {
        batsman_runs: batsmanRuns,noball_runs: noBalls,bowler: bowlerName,wide_runs: wideRuns} = currentElement;
        
      let totalRunsConceeded =Number(wideRuns) + Number(noBalls) + Number(batsmanRuns);

      if (economicalData[bowlerName] === undefined) {
        economicalData[bowlerName] = {};
        economicalData[bowlerName]["totalRunsConceeded"] = 0;
        economicalData[bowlerName]["totalBalls"] = 0;
      }
      if (Number(wideRuns) || Number(noBalls)) {
        economicalData[bowlerName]["totalBalls"]--;
      }
      economicalData[bowlerName]["totalRunsConceeded"] += totalRunsConceeded;
      economicalData[bowlerName]["totalBalls"]++;
    }
    return economicalData;
  }, {});

  return economicalData;
};
console.log(economicalBowler(matchesObject, deliveryData, 2015));

//Getting the bowlers data of runs and balls and calculating each bowlers economy

let EconomyData = (bowlerData) => {
  let economyResult = [];
  if (typeof deliveryData !== "object") {
    throw new Error("Input data type error");
  }

  for (const [key, value] of Object.entries(bowlerData)) {
    let overs = value["totalBalls"] / 6;
    let runs = value["totalRunsConceeded"];
    let economy = runs / overs;
    economyResult.push([key, economy]);
  }

  return economyResult;
};

//Final list of top ten economical bowlers

const topTenBestEconomy = (matchesObject, deliveryData, targetYear) => {
  let id = trackYearId(matchesObject, targetYear);

  let bowlerData = economicalBowler(matchesObject, deliveryData, targetYear);

  let bestEconomy = EconomyData(bowlerData);

  let listOfTopBowlersEconomy = bestEconomy
    .sort((bowlerA, bowlerB) => bowlerA[1] - bowlerB[1])
    .slice(0, 10)
    .map((bowler) => bowler[0]);

  return listOfTopBowlersEconomy;
};

module.exports = { topTenBestEconomy };
