/* ------------ Problem statement 2: Number of matches won per team per year in IPL.--------------*/
let {filePathInfo} = require('../filePath/filePathInfo.js');
 let matchesObject = require(filePathInfo["matchesJSONFilePath"])

const matchesWonPerTeamPerYear = (matchesObject) => {
    let teamObject = {};
    let matchesYear = "";
    let winnerTeam = "";
    if (typeof matchesObject !== "object") {
      throw new Error("Input type is not an object");
    }
    matchesObject.map((element) => {
      matchesYear = element["season"];
      winnerTeam = element["winner"];
      if (teamObject[matchesYear] === undefined) {
        teamObject[matchesYear] = {};
      }
      teamObject[matchesYear].hasOwnProperty(winnerTeam)
        ? teamObject[matchesYear][winnerTeam]++
        : (teamObject[matchesYear][winnerTeam] = 1);
    });
  
    return teamObject;
  };
  
  module.exports = {matchesWonPerTeamPerYear};