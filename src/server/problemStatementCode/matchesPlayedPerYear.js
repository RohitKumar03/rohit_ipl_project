let {filePathInfo} = require('../filePath/filePathInfo.js');
 let matchesObject = require(filePathInfo["matchesJSONFilePath"])

/* ------------ Problem statement 1: Number of matches played per year for all the years in IPL. --------------*/

const matchesPlayedPerYear = (matchesObject) => {
  if (typeof matchesObject !== "object") {
    throw new Error("Input type is not an object");
  }

let matchesCountPerYear = matchesObject.reduce((matchesCountPerYear, currentElement)=>{
   let {season} = currentElement
   matchesCountPerYear.hasOwnProperty(season)? matchesCountPerYear[season]++ : matchesCountPerYear[season] =1
  return matchesCountPerYear
},{})
return matchesCountPerYear
}


module.exports = {matchesPlayedPerYear};